package ru.artezio.railway.dto;

import ru.artezio.railway.models.RoleModel;
import ru.artezio.railway.models.UserInfoModel;
import ru.artezio.railway.models.UserStatus;
import java.util.List;

public class UserDto {

    private Long id;
    private String login;
    private String password;
    private String email;
    private UserStatus status;
    private UserInfoModel userInfo;
    private List<RoleModel> roles;

    public UserDto() {}

    public UserDto(Long id, String login, String password, String email, UserStatus status, UserInfoModel userInfo,
                   List<RoleModel> roles) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.email = email;
        this.status = status;
        this.userInfo = userInfo;
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public UserStatus getStatus() {
        return status;
    }
    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public UserInfoModel getUserInfo() {
        return userInfo;
    }
    public void setUserInfo(UserInfoModel userInfo) {
        this.userInfo = userInfo;
    }

    public List<RoleModel> getRoles() {
        return this.roles;
    }
    public void setRoles(List<RoleModel> roles) {
        this.roles = roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserDto userDto = (UserDto) o;

        if (id != null ? !id.equals(userDto.id) : userDto.id != null) {
            return false;
        }
        if (login != null ? !login.equals(userDto.login) : userDto.login != null) {
            return false;
        }
        if (password != null ? !password.equals(userDto.password) : userDto.password != null) {
            return false;
        }
        if (email != null ? !email.equals(userDto.email) : userDto.email != null) {
            return false;
        }
        if (status != userDto.status) {
            return false;
        }
        if (roles != null ? !roles.equals(userDto.roles) : userDto.roles != null) {
            return false;
        }
        return userInfo != null ? userInfo.equals(userDto.userInfo) : userDto.userInfo == null;
    }

}