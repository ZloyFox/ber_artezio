package ru.artezio.railway.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.artezio.railway.dao.UserDao;
import ru.artezio.railway.dto.UserDto;
import ru.artezio.railway.exceptions.UserNotFoundException;
import ru.artezio.railway.models.RoleModel;
import ru.artezio.railway.models.UserModel;
import ru.artezio.railway.queries.QueryParameter;

import java.util.Collections;
import java.util.List;

@Service
public class UserServiceImpl extends BaseService implements UserService {

    @Autowired private UserDao userDao;
    @Autowired private SecurityService securityService;

    public UserDto get(Long id) {
        UserModel userModel = userDao.find(id);
        if (userModel != null) {
            return map(userModel, UserDto.class);
        } else {
            throw new UserNotFoundException();
        }
    }

    @Override
    public UserDto getByLogin(String login) {
        UserModel userModel = userDao.findByLogin(Collections.singletonList(new QueryParameter("login", login)));
        if (userModel != null) {
            return map(userModel, UserDto.class);
        } else {
            throw new UserNotFoundException();
        }
    }

    @Override
    public UserDto getByEmail(String email) {
        UserModel userModel = userDao.findByEmail(Collections.singletonList(new QueryParameter("email", email)));
        if (userModel != null) {
            return map(userModel, UserDto.class);
        } else {
            throw new UserNotFoundException();
        }
    }

    @Override
    public List list() {
        return mapToList(userDao.list(), UserDto.class);
    }

    @Override
    public Long save(UserDto userDto) {
        userDto.setPassword(securityService.encryptPassword(userDto.getPassword()));
        userDto.setRoles(Collections.singletonList(new RoleModel(2L, "ROLE_USER")));
        return userDao.save(map(userDto, UserModel.class));
    }

    @Override
    public UserDto update(UserDto userDto) {
        return map(userDao.update(map(userDto, UserModel.class)), UserDto.class);
    }

    @Override
    public void remove(Long id) {
        try {
            userDao.remove(id);
        } catch (IllegalArgumentException exception) {
            throw new UserNotFoundException();
        }
    }

}