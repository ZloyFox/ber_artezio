package ru.artezio.railway.services;

import ru.artezio.railway.dto.UserDto;
import java.util.List;

public interface UserService {

    UserDto get(Long id);
    UserDto getByLogin(String login);
    UserDto getByEmail(String email);
    List list();
    Long save(UserDto userDto);
    UserDto update(UserDto userDto);
    void remove(Long id);

}