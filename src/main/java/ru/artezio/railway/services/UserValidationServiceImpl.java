package ru.artezio.railway.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.artezio.railway.dao.UserDao;
import ru.artezio.railway.dto.UserDto;
import ru.artezio.railway.exceptions.UserEmailAlreadyExistsException;
import ru.artezio.railway.exceptions.UserLoginAlreadyExistsException;
import ru.artezio.railway.queries.QueryParameter;

import java.util.Collections;

@Service
public class UserValidationServiceImpl implements UserValidationService {

    @Autowired private UserDao userDao;

    @Override
    public void validateUserLogin(UserDto userDto) {
        if (userDao.findByLogin(Collections.singletonList(new QueryParameter("login", userDto.getLogin()))) != null) {
            throw new UserLoginAlreadyExistsException();
        }
    }

    @Override
    public void validateUserEmail(UserDto userDto) {
        if (userDao.findByEmail(Collections.singletonList(new QueryParameter("email", userDto.getEmail()))) != null) {
            throw new UserEmailAlreadyExistsException();
        }
    }

}
