package ru.artezio.railway.services;

import org.springframework.security.core.Authentication;
import ru.artezio.railway.models.TokenModel;
import ru.artezio.railway.security.CustomAuthenticationToken;

public interface SecurityService {

    TokenModel getById(String id);
    Authentication authorize(CustomAuthenticationToken authenticationToken);
    String generateTokenValue(String login);
    void checkTokenExpirationTime(TokenModel token);
    String encryptPassword(String password);

}
