package ru.artezio.railway.services;

import ru.artezio.railway.dto.UserDto;

public interface UserValidationService {

    void validateUserLogin(UserDto userDto);
    void validateUserEmail(UserDto userDto);

}
