package ru.artezio.railway.services;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseService {

    @Autowired private Mapper dozerMapper;

    <T, U> T map(U object, Class<T> typeClass) {
        return dozerMapper.map(object, typeClass);
    }

    <T, U> List<T> mapToList(List<U> objectList, Class<T> typeClass) {
        List<T> resultList = new ArrayList<T>(objectList.size());
        for (U object : objectList) {
            resultList.add(dozerMapper.map(object, typeClass));
        }
        return resultList;
    }

}