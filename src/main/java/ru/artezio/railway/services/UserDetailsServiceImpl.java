package ru.artezio.railway.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.artezio.railway.dao.UserDao;
import ru.artezio.railway.models.UserModel;
import ru.artezio.railway.queries.QueryParameter;

import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        List<QueryParameter> list = Collections.singletonList(new QueryParameter("login", login));
        UserModel userModel = userDao.findByLogin(list);
        return new User(userModel.getLogin(), userModel.getPassword(), userModel.getRoles());
    }

}