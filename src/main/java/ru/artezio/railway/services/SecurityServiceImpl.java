package ru.artezio.railway.services;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.artezio.railway.dao.SecurityDao;
import ru.artezio.railway.dao.UserDao;
import ru.artezio.railway.exceptions.TokenExpirationDateException;
import ru.artezio.railway.exceptions.TokenNotFoundException;
import ru.artezio.railway.exceptions.UserNotFoundException;
import ru.artezio.railway.models.TokenModel;
import ru.artezio.railway.models.UserModel;
import ru.artezio.railway.queries.QueryParameter;
import ru.artezio.railway.security.CustomAuthenticationToken;

import java.util.Collections;
import java.util.Date;

@Service
public class SecurityServiceImpl implements SecurityService {

    @Autowired private UserDao userDao;
    @Autowired private SecurityDao securityDao;
    @Autowired private Date tokenExpirationDate;
    @Autowired private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public TokenModel getById(String id) {
        TokenModel token = securityDao.findById(id);
        if (token != null) {
            return token;
        } else {
            throw new TokenNotFoundException();
        }
    }

    @Override
    public Authentication authorize(CustomAuthenticationToken authenticationToken) {
        TokenModel token = getById(authenticationToken.getCredentials());
        checkTokenExpirationTime(token);
        UserModel userModel = userDao.find(token.getUserId());
        if (userModel != null) {
            return new CustomAuthenticationToken(token.getId(), userModel.getRoles());
        } else {
            throw new UserNotFoundException();
        }
    }

    @Override
    public String generateTokenValue(String login) {
        String sequence = "abcdefghijklmnopqrstuvwxyz1234567890";
        UserModel user = userDao.findByLogin(Collections.singletonList(new QueryParameter("login", login)));
        if (user != null) {
            return securityDao.save(new TokenModel(RandomStringUtils.random(36, sequence), user.getId(), this.tokenExpirationDate));
        } else {
            throw new UserNotFoundException();
        }
    }

    @Override
    public void checkTokenExpirationTime(TokenModel token) {
        if (token.getExpirationDate().before(new Date())) {
            throw new TokenExpirationDateException();
        }
    }

    @Override
    public String encryptPassword(String password) {
        return bCryptPasswordEncoder.encode(password);
    }

}
