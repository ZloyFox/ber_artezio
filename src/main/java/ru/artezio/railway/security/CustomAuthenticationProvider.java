package ru.artezio.railway.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import ru.artezio.railway.services.SecurityService;

public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Autowired private SecurityService securityService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        return securityService.authorize((CustomAuthenticationToken) authentication);
    }

    @Override
    public boolean supports(Class<?> authenticationClass) {
        return CustomAuthenticationToken.class.isAssignableFrom(authenticationClass);
    }

}
