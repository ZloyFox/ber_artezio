package ru.artezio.railway.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class CustomAuthenticationToken extends AbstractAuthenticationToken {

    private String tokenValue;

    public CustomAuthenticationToken(String tokenValue, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.tokenValue = tokenValue;
        super.setAuthenticated(true);
    }

    public CustomAuthenticationToken(String tokenValue) {
        this(tokenValue, null);
    }

    @Override
    public String getCredentials() {
        return this.tokenValue;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CustomAuthenticationToken that = (CustomAuthenticationToken) o;

        return tokenValue != null ? tokenValue.equals(that.tokenValue) : that.tokenValue == null;
    }

}
