package ru.artezio.railway.dao;

import ru.artezio.railway.models.UserModel;
import ru.artezio.railway.queries.QueryParameter;
import java.util.List;

public interface UserDao {

    UserModel find(Long id);
    List list();
    UserModel findByLogin(List<QueryParameter> params);
    UserModel findByEmail(List<QueryParameter> params);
    List<UserModel> findListByParams(List<QueryParameter> params);
    Long save(UserModel user);
    UserModel update(UserModel user);
    void remove(Long id);

}