package ru.artezio.railway.dao;

import org.springframework.stereotype.Repository;
import ru.artezio.railway.models.TokenModel;

@Repository
public class SecurityDaoImpl extends GenericDaoImpl<TokenModel, String> implements SecurityDao {

    @Override
    public TokenModel findById(String id) {
        return super.findById(id);
    }

    @Override
    public String save(TokenModel token) {
        return super.persist(token);
    }

    @Override
    public TokenModel update(TokenModel token) {
        return super.update(token);
    }

}
