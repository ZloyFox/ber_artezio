package ru.artezio.railway.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.artezio.railway.models.BaseModel;
import ru.artezio.railway.queries.QueryParameter;

import javax.persistence.*;
import java.lang.reflect.ParameterizedType;
import java.util.List;

@Repository
@Transactional
public abstract class GenericDaoImpl<T extends BaseModel<U>, U> {

    @PersistenceContext private EntityManager entityManager;
    private Class<T> entityClass;

    @SuppressWarnings("unchecked")
    public GenericDaoImpl() {
        this.entityClass = (Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    T findById(U id) {
        return entityManager.find(entityClass, id);
    }

    List list() {
        return entityManager.createQuery("from " + entityClass.getSimpleName() + " c").getResultList();
    }

    @SuppressWarnings("unchecked")
    T findObjectByNamedQuery(String queryName, List<QueryParameter> params) {
        try {
            return (T) buildNamedQuery(queryName, params).getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    List<T> findObjectsByNamedQuery(String queryName, List<QueryParameter> params) {
        return buildNamedQuery(queryName, params).getResultList();
    }

    @SuppressWarnings("unchecked")
    U persist(T model) {
        entityManager.persist(model);
        return model.getId();
    }

    @SuppressWarnings("unchecked")
    T update(T model) {
        return entityManager.merge(model);
    }

    void remove(U id) {
        entityManager.remove(findById(id));
    }

    private Query buildNamedQuery(String queryName, List<QueryParameter> params) {
        Query query = entityManager.createNamedQuery(queryName);
        for (QueryParameter parameter : params) {
            query.setParameter(parameter.name, parameter.value);
        }
        return query;
    }

}