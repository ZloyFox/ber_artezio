package ru.artezio.railway.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.artezio.railway.models.UserModel;
import ru.artezio.railway.queries.QueryParameter;
import java.util.List;

@Repository
public class UserDaoImpl extends GenericDaoImpl<UserModel, Long> implements UserDao {

    @Override
    public UserModel find(Long id) {
        return super.findById(id);
    }

    @Override
    public List list() {
        return super.list();
    }

    @Override
    public List<UserModel> findListByParams(List<QueryParameter> params) {
        return super.findObjectsByNamedQuery("UserListByStatus", params);
    }

    @Override
    public UserModel findByLogin(List<QueryParameter> params) {
        return super.findObjectByNamedQuery("UserByLogin", params);
    }

    @Override
    public UserModel findByEmail(List<QueryParameter> params) {
        return super.findObjectByNamedQuery("UserByEmail", params);
    }

    @Override
    public Long save(UserModel user) {
        return super.persist(user);
    }

    @Override
    public UserModel update(UserModel user) {
        return super.update(user);
    }

    @Override
    public void remove(Long id) {
        super.remove(id);
    }

}