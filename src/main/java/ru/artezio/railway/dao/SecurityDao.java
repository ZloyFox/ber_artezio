package ru.artezio.railway.dao;

import ru.artezio.railway.models.TokenModel;

public interface SecurityDao {

    TokenModel findById(String id);
    String save(TokenModel token);
    TokenModel update(TokenModel token);

}
