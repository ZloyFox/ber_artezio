package ru.artezio.railway.dao;

import org.springframework.stereotype.Repository;
import ru.artezio.railway.exceptions.RoleNotFoundException;
import ru.artezio.railway.models.RoleModel;
import java.util.List;

@Repository
public class RoleDaoImpl extends GenericDaoImpl<RoleModel, Long> implements RoleDao {

    @Override
    public RoleModel find(Long id) {
        return super.findById(id);
    }

    @Override
    public List list() {
        return super.list();
    }

    @Override
    public Long save(RoleModel role) {
        return super.persist(role);
    }

    @Override
    public RoleModel update(RoleModel role) {
        return super.update(role);
    }

    @Override
    public void remove(Long id) {
        try {
            super.remove(id);
        } catch (IllegalArgumentException exception) {
            throw new RoleNotFoundException();
        }
    }

}