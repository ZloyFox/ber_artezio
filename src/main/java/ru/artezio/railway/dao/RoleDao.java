package ru.artezio.railway.dao;

import ru.artezio.railway.models.RoleModel;
import java.util.List;

public interface RoleDao {

    RoleModel find(Long id);
    List list();
    Long save(RoleModel role);
    RoleModel update(RoleModel role);
    void remove(Long id);

}
