package ru.artezio.railway.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.artezio.railway.dto.UserDto;
import ru.artezio.railway.exceptions.ApiException;
import ru.artezio.railway.services.UserService;
import ru.artezio.railway.services.UserValidationService;

@Controller
@RequestMapping
public class AuthorizationController extends BaseController {

    @Autowired private UserValidationService userValidationService;
    @Autowired private UserService userService;

    @PostMapping(value = "/check/userLogin")
    public ResponseEntity checkUserLogin(@RequestBody UserDto userDto) throws JsonProcessingException {
        try {
            userValidationService.validateUserLogin(userDto);
            return createSuccessResponse();
        } catch (ApiException exception) {
            return createFailedResponse(exception);
        } catch (Exception ex) {
            return createInternalErrorResponse();
        }
    }

    @PostMapping(value = "/check/userEmail")
    public ResponseEntity checkUserEmail(@RequestBody UserDto userDto) throws JsonProcessingException {
        try {
            userValidationService.validateUserEmail(userDto);
            return createSuccessResponse();
        } catch (ApiException exception) {
            return createFailedResponse(exception);
        } catch (Exception ex) {
            return createInternalErrorResponse();
        }
    }

    @PostMapping(value = "/registration")
    public ResponseEntity registration(@RequestBody UserDto userDto) throws JsonProcessingException {
        try {
            return createSuccessResponse(userService.save(userDto));
        } catch (ApiException exception) {
            return createFailedResponse(exception);
        } catch (Exception ex) {
            return createInternalErrorResponse();
        }
    }

}