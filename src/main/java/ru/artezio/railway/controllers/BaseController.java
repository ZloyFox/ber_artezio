package ru.artezio.railway.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.artezio.railway.controllers.responses.BaseResponse;
import ru.artezio.railway.controllers.responses.FailedResponse;
import ru.artezio.railway.controllers.responses.SuccessResponse;
import ru.artezio.railway.exceptions.ApiException;
import ru.artezio.railway.exceptions.InternalServerErrorException;

public abstract class BaseController {

    ResponseEntity createSuccessResponse() throws JsonProcessingException {
        return new ResponseEntity<BaseResponse>(new BaseResponse(true), HttpStatus.OK);
    }

    ResponseEntity createSuccessResponse(Object object) throws JsonProcessingException {
        return new ResponseEntity<SuccessResponse>(new SuccessResponse(object), HttpStatus.OK);
    }

    ResponseEntity createFailedResponse(ApiException ex) throws JsonProcessingException {
        return new ResponseEntity<FailedResponse>(new FailedResponse(ex), HttpStatus.OK);
    }

    ResponseEntity createInternalErrorResponse() {
        return new ResponseEntity<FailedResponse>(new FailedResponse(new InternalServerErrorException()), HttpStatus.OK);
    }

}