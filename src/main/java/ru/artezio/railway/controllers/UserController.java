package ru.artezio.railway.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.artezio.railway.dto.UserDto;
import ru.artezio.railway.exceptions.ApiException;
import ru.artezio.railway.services.UserService;

@Controller
@RequestMapping(value = "/api/users")
public class UserController extends BaseController {

    @Autowired private UserService userService;

    @GetMapping("/{id}")
    public ResponseEntity getUser(@PathVariable Long id) throws JsonProcessingException {
        try {
            return createSuccessResponse(userService.get(id));
        } catch (ApiException ex) {
            return createFailedResponse(ex);
        } catch (Exception ex) {
            return createInternalErrorResponse();
        }
    }

    @GetMapping
    public ResponseEntity getUserList() throws JsonProcessingException {
        try {
            return createSuccessResponse(userService.list());
        } catch (ApiException ex) {
            return createFailedResponse(ex);
        } catch (Exception ex) {
            return createInternalErrorResponse();
        }
    }

    @PostMapping
    public ResponseEntity saveUser(@RequestBody UserDto userDto) throws JsonProcessingException {
        try {
            return createSuccessResponse(userService.save(userDto));
        } catch (ApiException ex) {
            return createFailedResponse(ex);
        } catch (Exception ex) {
            return createInternalErrorResponse();
        }
    }

    @PutMapping
    public ResponseEntity updateUser(@RequestBody UserDto userDto) throws JsonProcessingException {
        try {
            return createSuccessResponse(userService.update(userDto));
        } catch (ApiException ex) {
            return createFailedResponse(ex);
        } catch (Exception ex) {
            return createInternalErrorResponse();
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteUser(@PathVariable Long id) throws JsonProcessingException {
        try {
            userService.remove(id);
            return createSuccessResponse();
        } catch (ApiException ex) {
            return createFailedResponse(ex);
        } catch (Exception ex) {
            return createInternalErrorResponse();
        }
    }

}