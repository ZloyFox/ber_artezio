package ru.artezio.railway.exceptions;

public class UserLoginAlreadyExistsException extends ApiException {

    public UserLoginAlreadyExistsException() {
        super("LOGIN_ALREADY_EXISTS", "Пользователь с таким логином уже существует!");
    }

}
