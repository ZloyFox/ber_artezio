package ru.artezio.railway.exceptions;

public class RoleAlreadyExistsException extends ApiException {

    public RoleAlreadyExistsException() {
        super("ROLE_ALREADY_EXISTS", "Данная роль уже существует!");
    }

}