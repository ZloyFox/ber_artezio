package ru.artezio.railway.exceptions;

public abstract class ApiException extends RuntimeException {

    private final String code;
    private final String message;

    ApiException(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

}