package ru.artezio.railway.exceptions;

public class InternalServerErrorException extends ApiException {

    public InternalServerErrorException() {
        super("INTERNAL_SERVER_ERROR", "Упс, что-то пошло не так :(");
    }

}