package ru.artezio.railway.exceptions;

public class RoleNotFoundException extends ApiException {

    public RoleNotFoundException() {
        super("ROLE_NOT_FOUND", "Роль не найдена!");
    }

}