package ru.artezio.railway.exceptions;

public class UserAlreadyExistsException extends ApiException {

    public UserAlreadyExistsException() {
        super("USER_ALREADY_EXISTS", "Пользователь уже существует!");
    }

}