package ru.artezio.railway.exceptions;

public class TokenNotFoundException extends ApiException {

    public TokenNotFoundException() {
        super("TOKEN_NOT_FOUND", "Токен не найден!");
    }

}
