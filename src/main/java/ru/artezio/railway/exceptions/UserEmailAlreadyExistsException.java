package ru.artezio.railway.exceptions;

public class UserEmailAlreadyExistsException extends ApiException {

    public UserEmailAlreadyExistsException() {
        super("EMAIL_ALREADY_EXISTS", "Данный e-mail уже занят!");
    }

}
