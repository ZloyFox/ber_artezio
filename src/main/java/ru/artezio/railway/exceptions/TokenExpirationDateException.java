package ru.artezio.railway.exceptions;

public class TokenExpirationDateException extends ApiException {

    public TokenExpirationDateException() {
        super("TOKEN_EXPIRED", "Время действия ключа истекло!");
    }

}
