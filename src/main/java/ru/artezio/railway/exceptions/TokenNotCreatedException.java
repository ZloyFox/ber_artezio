package ru.artezio.railway.exceptions;

public class TokenNotCreatedException extends ApiException {

    public TokenNotCreatedException() {
        super("TOKEN_NOT_CREATED", "Токен не был создан!");
    }
}
