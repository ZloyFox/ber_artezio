package ru.artezio.railway.exceptions;

public class UserNotFoundException extends ApiException {

    public UserNotFoundException() {
        super("USER_NOT_FOUND", "Указанный пользователь не найден!");
    }

}