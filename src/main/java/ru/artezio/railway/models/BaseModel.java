package ru.artezio.railway.models;

public abstract class BaseModel<T> {

    public abstract T getId();
    public abstract void setId(T t);

    @Override
    public String toString() {
        return getClass().getSimpleName() + ", ID: " + getId();
    }

}