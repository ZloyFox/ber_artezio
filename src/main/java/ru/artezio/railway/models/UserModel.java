package ru.artezio.railway.models;

import javax.persistence.*;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "UserListByStatus", query = "select user from UserModel user where user.status = :status"),
        @NamedQuery(name = "UserByLogin", query = "select user from UserModel user where user.login = :login"),
        @NamedQuery(name = "UserByEmail", query = "select user from UserModel user where user.email = :email")
})
@Table(name = "users")
public class UserModel extends BaseModel<Long> {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) private Long id;
    @Column private String login;
    @Column private String password;
    @Column private String email;
    @Column @Enumerated(EnumType.STRING) private UserStatus status;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY) @PrimaryKeyJoinColumn private UserInfoModel userInfo;
    @ManyToMany @JoinTable(
            name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")) private List<RoleModel> roles;

    public UserModel() {
    }

    public UserModel(Long id, String login, String password, String email, UserStatus status, UserInfoModel userInfo,
                     List<RoleModel> roles) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.email = email;
        this.status = status;
        this.userInfo = userInfo;
        this.roles = roles;
    }

    @Override
    public Long getId() {
        return id;
    }
    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public UserStatus getStatus() {
        return status;
    }
    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public UserInfoModel getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfoModel userInfo) {
        this.userInfo = userInfo;
    }

    public List<RoleModel> getRoles() {
        return this.roles;
    }

    public void setRoles(List<RoleModel> roles) {
        this.roles = roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserModel userModel = (UserModel) o;

        if (id != null ? !id.equals(userModel.id) : userModel.id != null) {
            return false;
        }
        if (login != null ? !login.equals(userModel.login) : userModel.login != null) {
            return false;
        }
        if (password != null ? !password.equals(userModel.password) : userModel.password != null) {
            return false;
        }
        if (email != null ? !email.equals(userModel.email) : userModel.email != null) {
            return false;
        }
        if (status != userModel.status) {
            return false;
        }
        if (userInfo != null ? !userInfo.equals(userModel.userInfo) : userModel.userInfo != null) {
            return false;
        }
        return roles != null ? roles.equals(userModel.roles) : userModel.roles == null;
    }

}