package ru.artezio.railway.models;

import org.springframework.security.core.GrantedAuthority;
import javax.persistence.*;

@Entity
@Table(name = "roles")
public class RoleModel extends BaseModel<Long> implements GrantedAuthority {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) private Long id;
    @Column private String name;

    public RoleModel() {
    }

    public RoleModel(String name) {
        this.name = name;
    }

    public RoleModel(Long id, String name) {
        this(name);
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }
    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getAuthority() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleModel role = (RoleModel) o;

        if (id != null ? !id.equals(role.id) : role.id != null) return false;
        return name != null ? name.equals(role.name) : role.name == null;
    }

}