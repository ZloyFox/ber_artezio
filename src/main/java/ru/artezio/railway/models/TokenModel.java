package ru.artezio.railway.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_tokens")
public class TokenModel extends BaseModel<String> {

    @Id private String id;
    @Column(name = "user_id") private Long userId;
    @Column(name = "expiration_date") private Date expirationDate;

    public TokenModel() {
    }

    public TokenModel(String id, Long userId, Date expirationDate) {
        this.id = id;
        this.userId = userId;
        this.expirationDate = expirationDate;
    }

    @Override
    public String getId() {
        return id;
    }
    @Override
    public void setId(String id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }
    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TokenModel token = (TokenModel) o;

        if (id != null ? !id.equals(token.id) : token.id != null) return false;
        if (expirationDate != null ? !expirationDate.equals(token.expirationDate) : token.expirationDate != null) return false;
        return userId != null ? userId.equals(token.userId) : token.userId == null;
    }

}