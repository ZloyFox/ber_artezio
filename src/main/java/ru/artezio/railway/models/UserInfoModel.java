package ru.artezio.railway.models;

import javax.persistence.*;

@Entity
@Table(name = "user_info")
public class UserInfoModel extends BaseModel<Long> {

    @Id @Column(name = "user_id") private Long userId;
    @Column(name = "first_name") private String firstName;
    @Column(name = "middle_name") private String middleName;
    @Column(name = "last_name") private String lastName;
    @Column private Integer age;
    @Column private String city;

    public UserInfoModel() {
    }

    public UserInfoModel(Long userId, String firstName, String middleName, String lastName, Integer age, String city) {
        this.userId = userId;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.age = age;
        this.city = city;
    }

    @Override
    public Long getId() {
        return this.userId;
    }
    @Override
    public void setId(Long userId) {
        this.userId = this.userId;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserInfoModel that = (UserInfoModel) o;

        if (userId != null ? !userId.equals(that.userId) : that.userId != null) {
            return false;
        }
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) {
            return false;
        }
        if (middleName != null ? !middleName.equals(that.middleName) : that.middleName != null) {
            return false;
        }
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) {
            return false;
        }
        if (age != null ? !age.equals(that.age) : that.age != null) {
            return false;
        }
        return city != null ? city.equals(that.city) : that.city == null;
    }

}
