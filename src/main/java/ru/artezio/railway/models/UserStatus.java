package ru.artezio.railway.models;

public enum UserStatus {

    ACTIVE, INACTIVE, DONT_DISTURB, AWAY, BLOCKED;

}