CREATE TABLE users (
  id    INTEGER PRIMARY KEY AUTO_INCREMENT,
  login  VARCHAR(20),
  password VARCHAR(255),
  email VARCHAR(20),
  status VARCHAR(20)
);

CREATE TABLE roles (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(20)
);

CREATE TABLE user_roles (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  user_id INTEGER,
  role_id INTEGER
);

CREATE TABLE user_tokens (
  id VARCHAR(50) PRIMARY KEY ,
  expiration_date DATETIME,
  user_id INTEGER
);

INSERT INTO users (id, login, password, email, status) VALUES (1, '1', '1', '1', 'ACTIVE');
INSERT INTO users (id, login, password, email, status) VALUES (2, '2', '2', '2', 'ACTIVE');
INSERT INTO users (id, login, password, email, status) VALUES (3, '3', '3', '3', 'ACTIVE');
INSERT INTO users (id, login, password, email, status) VALUES (4, '4', '4', '4', 'ACTIVE');

INSERT INTO roles (id, name) VALUES (1, 'ROLE_ADMIN');
INSERT INTO roles (id, name) VALUES (2, 'ROLE_USER');

INSERT INTO user_roles (id, user_id, role_id) VALUES (1, 1, 1);
INSERT INTO user_roles (id, user_id, role_id) VALUES (2, 2, 2);
INSERT INTO user_roles (id, user_id, role_id) VALUES (3, 3, 2);
INSERT INTO user_roles (id, user_id, role_id) VALUES (4, 4, 2);

INSERT INTO user_tokens (id, expiration_date, user_id) VALUES ('1', '2018-04-19 09:00:32', 1);
INSERT INTO user_tokens (id, expiration_date, user_id) VALUES ('2', '2018-04-21 09:00:32', 2);