<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">

        <t:resources />
    </head>
    <body>
        <div class="row">
        </div>
        <div id="authorization-form" class="container">
            <form action="<c:url value="/login"/>" method="post" >
                <legend>Вход в систему</legend>
                <fieldset id="inputs">
                    <div class="row">
                        <label for="login">Логин:</label>
                        <input type="text" id="login" name="login" class="form-control" autofocus>
                        <label for="password">Пароль:</label>
                        <input type="password" id="password" name="password" class="form-control">
                    </div>
                </fieldset><br>
                <fieldset id="action">
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="submit" class="btn btn-primary" value="Вход">
                        </div>
                        <div class="col-sm-6">
                            <input type="button" class="btn btn-primary" value="Регистрация">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </body>
</html>
