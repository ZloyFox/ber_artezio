<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">

        <t:resources />
    </head>
    <body>
        <div id="registration-field" class="container">
            <form action="/registration" method="post">
                <fieldset id="inputs">
                    <legend>Регистрация</legend>
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label for="first_name">Имя:</label>
                            <input type="text" id="first_name" name="first_name" class="form-control" autofocus>
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="middle_name">Фамилия:</label>
                            <input type="text" id="middle_name" name="middle_name" class="form-control">
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="last_name">Отчество:</label>
                            <input type="text" id="last_name" name="last_name" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="login">Логин:</label>
                            <input type="text" id="login" name="login" class="form-control" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="password">Пароль:</label>
                            <input type="password" id="password" name="password" class="form-control" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="repass">Повторите пароль:</label>
                            <input type="password" id="repass" class="form-control" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="email">E-mail:</label>
                            <input type="email" id="email" name="email" class="form-control" placeholder="something@example.com">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="age">Возраст:</label>
                            <input type="number" id="age" name="age" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="city">Город:</label>
                            <input type="text" id="city" name="city" class="form-control">
                        </div>
                    </div>
                </fieldset>
                <fieldset id="action">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <input type="submit" id="checkin" class="btn btn-primary" value="Регистрация">
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </body>
</html>
