function get(url) {
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        async: true,
        success: function(response) {
            var str = '';
            if(response['success'] === true) {
                var result = response['result'];
                if(result instanceof Array)
                    for(var i = 0; i < result.length; i++)
                        str += 'id: ' + result[i]['id'] + ', login: ' + result[i]['login'] +
                            ', e-mail: ' + result[i]['email'] + ', status: ' + result[i]['status'] +
                            '<br>';
                else
                    str = 'id: ' + result['id'] + ', login: ' + result['login'] + ', e-mail: ' +
                        result['email'] + ', status: ' + result['status'];
            }
            else
                str = '';
            document.write(str);
        },
        error: function(jqXHR) {
        }
    })
}

function del(url) {
    $.ajax({
        type: 'DELETE',
        url: url,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        success: function (result) {
            document.write('код ' + result['code'] + ', описание: ' + result['description']);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            document.write(jqXHR.responseText)
        }
    })
}

function update(url) {
    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'json',
        async: true,
        success: function (result) {
            document.write(result);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status + ' ' + jqXHR.responseText);
        }
    })
}