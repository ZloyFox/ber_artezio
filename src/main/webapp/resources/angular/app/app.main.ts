import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {AuthorizationModule} from './autorization.module';
import {RegistrationModule} from './registration.module';

platformBrowserDynamic().bootstrapModule(AuthorizationModule);
platformBrowserDynamic().bootstrapModule(RegistrationModule);