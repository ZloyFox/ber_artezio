import {Component} from "@angular/core";
import {Http , Headers , RequestOptions , Response , RequestMethod} from '@angular/http';

@Component({
    selector: 'authorization',
    template: `
        <div id="authorization-form" class="container">
            <form (ngSubmit)="post()">
                <legend>Вход в систему</legend>
                <fieldset id="inputs">
                    <div class="row">
                        <label for="login">Логин:</label>
                        <input type="text" id="login" name="login" [(ngModel)]="login" class="form-control" autofocus>
                        <label for="password">Пароль:</label>
                        <input type="password" id="password" name="password" [(ngModel)]="password" class="form-control">
                    </div>
                </fieldset><br>
                <fieldset id="action">
                    <input type="submit" class="btn btn-primary" value="Вход">
                </fieldset>
            </form>
        </div>   
    `
})
export class AuthorizationComponent{

    public login : string;
    public password : string;

    constructor(public http: Http) {}

    post() {
        let body = 'login='+this.login+'&password='+this.password;
        let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        return this.http.post('http://localhost/login', body ,{ headers: headers })
            .subscribe(
                data => {
                    let result = data.json();
                    if (result.success === true) {
                        alert(result.result);
                    } else {
                        alert(result.message);
                    }
                },
            );
    }
}