import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { RegistrationComponent } from './registration.component';

@NgModule({
    imports: [ BrowserModule, FormsModule, HttpModule ],
    declarations: [ RegistrationComponent ],
    bootstrap: [ RegistrationComponent ]
})
export class RegistrationModule {
}