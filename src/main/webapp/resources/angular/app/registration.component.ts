import { Component } from '@angular/core';
import { Http } from '@angular/http';

@Component({
    selector: 'registration',
    template: `
        <div id="registration-field" class="container">
            <form (ngSubmit)="registration()">
                <fieldset id="inputs">
                    <legend>Регистрация</legend>
                    <div class="row">
                        <div class="form-group col-sm-4">
                            <label for="first_name">Имя:</label>
                            <input type="text" id="first_name" name="first_name" class="form-control" [(ngModel)]="first_name" autofocus>
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="middle_name">Фамилия:</label>
                            <input type="text" id="middle_name" name="middle_name" class="form-control" [(ngModel)]="middle_name">
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="last_name">Отчество:</label>
                            <input type="text" id="last_name" name="last_name" class="form-control" [(ngModel)]="last_name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="login">Логин:</label>
                            <input type="text" id="login" name="login" class="form-control" (blur)="checkUserLogin($event)" [(ngModel)]="login"  required>
                            <div class="alert alert-danger" *ngIf="loginValidationMessage">
                                {{loginValidationMessage}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="password">Пароль:</label>
                            <input type="password" id="password" name="password" class="form-control" [(ngModel)]="password" required>
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="repass">Повторите пароль:</label>
                            <input type="password" id="repass" class="form-control" required>
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="email">E-mail:</label>
                            <input type="email" id="email" name="email" class="form-control" (blur)="checkUserEmail($event)" placeholder="something@example.com" [(ngModel)]="email">
                            <div class="alert alert-danger" *ngIf="emailValidationMessage">
                                {{emailValidationMessage}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="age">Возраст:</label>
                            <input type="number" id="age" name="age" class="form-control" [(ngModel)]="age">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="city">Город:</label>
                            <input type="text" id="city" name="city" class="form-control" [(ngModel)]="city">
                        </div>
                    </div>
                </fieldset>
                <fieldset id="action">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <input type="submit" id="checkin" class="btn btn-primary" value="Регистрация">
                        </div>
                    </div>
                </fieldset>
            </form>               
        </div>
    `
})
export class RegistrationComponent {

    public first_name : string;
    public middle_name : string;
    public last_name : string;
    public login : string;
    public password : string;
    public email : string;
    public city: string;
    public age : number;

    public loginValidationMessage: string;
    public emailValidationMessage: string;

    constructor(public http: Http) {}

    checkUserLogin(event) {
        this.http.post('http://localhost/check/userLogin', { login: this.login })
            .subscribe(
                data => {
                    let result = data.json();

                    if (result.success === true) {
                        this.loginValidationMessage = '';
                    } else {
                        this.loginValidationMessage = result.message;
                    }
                },
            );
    }

    checkUserEmail(event) {
        this.http.post('http://localhost/check/userEmail', { email: this.email })
            .subscribe(
                data => {
                    let result = data.json();

                    if (result.success === true) {
                        this.emailValidationMessage = '';
                    } else {
                        this.emailValidationMessage = result.message;
                    }
                },
            );
    }

    registration() {
        let user = {
            first_name: this.first_name,
            middle_name: this.middle_name,
            last_name: this.last_name,
            login: this.login,
            password: this.password,
            email: this.email,
            city: this.city,
            age : this.age
        };
        this.http.post('http://localhost/registration', user)
            .subscribe(
                data => {
                    let result = data.json();
                    if (result.success === true) {
                        alert('Регистрация прошла успешно!');
                    } else {
                        alert(result.message);
                    }
                }
            )
    }

}

