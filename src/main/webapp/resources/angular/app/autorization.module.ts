import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import {AuthorizationComponent} from "./authorization.component";

@NgModule({
    imports: [ BrowserModule, FormsModule, HttpModule ],
    declarations: [ AuthorizationComponent ],
    bootstrap: [ AuthorizationComponent ]
})
export class AuthorizationModule {
}