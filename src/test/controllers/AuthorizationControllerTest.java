package controllers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.artezio.railway.controllers.AuthorizationController;
import ru.artezio.railway.controllers.responses.BaseResponse;
import ru.artezio.railway.controllers.responses.FailedResponse;
import ru.artezio.railway.controllers.responses.SuccessResponse;
import ru.artezio.railway.dto.UserDto;
import ru.artezio.railway.exceptions.UserEmailAlreadyExistsException;
import ru.artezio.railway.exceptions.UserLoginAlreadyExistsException;
import ru.artezio.railway.models.RoleModel;
import ru.artezio.railway.models.UserStatus;
import ru.artezio.railway.services.UserService;
import ru.artezio.railway.services.UserValidationService;

import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AuthorizationControllerTest extends BaseControllerTest {

    @InjectMocks @Autowired private AuthorizationController authorizationController;
    @Mock private UserService userService;
    @Mock private UserValidationService userValidationService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(authorizationController).build();
    }

    @Test
    public void checkUserLoginUserNotExistsReturnSuccessResponse() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setLogin("0");

        doNothing().when(userValidationService).validateUserLogin(userDto);

        mockMvc.perform(post("/check/userLogin")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userDto)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json;charset=UTF-8"))
                    .andExpect(content().bytes(objectMapper.writeValueAsBytes(new BaseResponse(true))));
    }

    @Test
    public void checkUserLoginUserExistsReturnFailedResponseWithUserLoginAlreadyExistsException() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setLogin("1");

        doThrow(new UserLoginAlreadyExistsException()).when(userValidationService).validateUserLogin(userDto);

        mockMvc.perform(post("/check/userLogin")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userDto)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json;charset=UTF-8"))
                    .andExpect(content().bytes(objectMapper.writeValueAsBytes(new FailedResponse(new UserLoginAlreadyExistsException()))));
    }

    @Test
    public void checkUserEmailUserNotExistsReturnSuccessResponse() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setEmail("0");

        doNothing().when(userValidationService).validateUserEmail(userDto);

        mockMvc.perform(post("/check/userEmail")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userDto)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json;charset=UTF-8"))
                    .andExpect(content().bytes(objectMapper.writeValueAsBytes(new BaseResponse(true))));
    }

    @Test
    public void checkUserEmailUserExistsReturnFailedResponseWithUserEmailAlreadyExistsException() throws Exception {
        UserDto userDto = new UserDto();
        userDto.setEmail("1");

        doThrow(new UserEmailAlreadyExistsException()).when(userValidationService).validateUserEmail(userDto);

        mockMvc.perform(post("/check/userEmail")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userDto)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json;charset=UTF-8"))
                    .andExpect(content().bytes(objectMapper.writeValueAsBytes(new FailedResponse(new UserEmailAlreadyExistsException()))));
    }

    @Test
    public void registration() throws Exception {
        UserDto userDto = new UserDto(null, "1", "1", "1", UserStatus.ACTIVE, null, Collections.singletonList(new RoleModel(2L, "ROLE_USER")));

        when(userService.save(userDto)).thenReturn(1L);

        mockMvc.perform(post("/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userDto)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json;charset=UTF-8"))
                    .andExpect(content().bytes(objectMapper.writeValueAsBytes(new SuccessResponse(1L))));
    }

}
