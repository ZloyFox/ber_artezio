package controllers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.artezio.railway.controllers.UserController;
import ru.artezio.railway.controllers.responses.BaseResponse;
import ru.artezio.railway.controllers.responses.FailedResponse;
import ru.artezio.railway.controllers.responses.SuccessResponse;
import ru.artezio.railway.dto.UserDto;
import ru.artezio.railway.exceptions.UserNotFoundException;
import ru.artezio.railway.models.RoleModel;
import ru.artezio.railway.models.UserStatus;
import ru.artezio.railway.services.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest extends BaseControllerTest {

    @InjectMocks @Autowired private UserController userController;
    @Mock private UserService userService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    public void getUserExistsReturnSuccessResponseWithFoundUser() throws Exception {
        UserDto userDto = createUserDto(1L, null);

        when(userService.get(correctId)).thenReturn(userDto);

        mockMvc.perform(get("/api/users/{id}", correctId))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().bytes(objectMapper.writeValueAsBytes(new SuccessResponse(userDto))));
    }

    @Test
    public void getUserNotExistsReturnFailedResponseUserNotFoundException() throws Exception {
        when(userService.get(incorrectId)).thenThrow(new UserNotFoundException());

        mockMvc.perform(get("/api/users/{id}", incorrectId))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().bytes(objectMapper.writeValueAsBytes(new FailedResponse(new UserNotFoundException()))));
    }

    @Test
    public void getListUsersExistReturnSuccessResponseWithUserList() throws Exception {
        List<UserDto> userDtoList = createUserDtoList();

        when(userService.list()).thenReturn(userDtoList);

        mockMvc.perform(get("/api/users/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().bytes(objectMapper.writeValueAsBytes(new SuccessResponse(userDtoList))));
    }

    @Test
    public void createUserNotExistsReturnSuccessResponseWithIdCreatedUser() throws Exception {
        UserDto userDto = createUserDtoWithoutId();

        when(userService.save(userDto)).thenReturn(1L);

        mockMvc.perform(post("/api/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userDto)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json;charset=UTF-8"))
                    .andExpect(content().bytes(objectMapper.writeValueAsBytes(new SuccessResponse(1L))));
    }

    @Test
    public void updateUserExistsReturnSuccessResponseWithUpdatedUser() throws Exception {
        UserDto userDto = createUserDto(1L, null);
        userDto.setLogin("newLogin");

        when(userService.update(userDto)).thenReturn(userDto);

        mockMvc.perform(put("/api/users/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(userDto)))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json;charset=UTF-8"))
                    .andExpect(content().bytes(objectMapper.writeValueAsBytes(new SuccessResponse(userDto))));
    }

    @Test
    public void removeUserExistsReturnSuccessResponse() throws Exception {
        doNothing().when(userService).remove(correctId);

        mockMvc.perform(delete("/api/users/{id}", correctId))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().bytes(objectMapper.writeValueAsBytes(new BaseResponse(true))));
    }

    @Test
    public void removeUserNotExistsReturnFailedResponseWithUserNotFoundException() throws Exception {
        doThrow(new UserNotFoundException()).when(userService).remove(incorrectId);

        mockMvc.perform(delete("/api/users/{id}", incorrectId))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().bytes(objectMapper.writeValueAsBytes(new FailedResponse(new UserNotFoundException()))));
    }

    private UserDto createUserDto(Long id, String password) {
        List<RoleModel> roleModelList = new ArrayList<RoleModel>();
        roleModelList.add(new RoleModel(1L, "ROLE_ADMIN"));

        return new UserDto(id, id.toString(), password, id.toString(), UserStatus.ACTIVE, null, roleModelList);
    }

    private UserDto createUserDtoWithoutId() {
        UserDto userDto = new UserDto();
        List<RoleModel> roleModelList = new ArrayList<RoleModel>();
        roleModelList.add(new RoleModel(1L, "ROLE_ADMIN"));

        Long randomNumber = new Random().nextLong();
        userDto.setLogin(randomNumber.toString());
        userDto.setPassword(randomNumber.toString());
        userDto.setEmail(randomNumber.toString());
        userDto.setStatus(UserStatus.DONT_DISTURB);
        userDto.setRoles(roleModelList);

        return userDto;
    }

    private List<UserDto> createUserDtoList() {
        List<UserDto> userDtoList = new ArrayList<UserDto>(4);
        List<RoleModel> roleModelList = new ArrayList<RoleModel>(1);
        roleModelList.add(new RoleModel(2L, "user"));

        for (int i = 0; i < 3; i++) {
            Long id = (long) i;
            userDtoList.add(new UserDto(id, id.toString(), id.toString(), id.toString(), UserStatus.ACTIVE, null, roleModelList));
        }
        userDtoList.add(new UserDto(4L, "4", "4", "4", UserStatus.AWAY, null, roleModelList));

        return userDtoList;
    }

}