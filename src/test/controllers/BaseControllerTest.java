package controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.artezio.railway.controllers.BaseController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:src/main/resources/test-config.xml",
        "file:src/main/resources/security-config.xml"})
public abstract class BaseControllerTest extends BaseController {

    MockMvc mockMvc;
    ObjectMapper objectMapper = new ObjectMapper();
    final Long correctId = 1L;
    final Long incorrectId = -1L;

}