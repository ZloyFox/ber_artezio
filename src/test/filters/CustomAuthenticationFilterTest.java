package filters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.artezio.railway.models.RoleModel;
import ru.artezio.railway.security.CustomAuthenticationFilter;
import ru.artezio.railway.security.CustomAuthenticationToken;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import java.io.IOException;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:src/main/resources/test-config.xml",
        "file:src/main/resources/security-config.xml"})
public class CustomAuthenticationFilterTest {

    @Autowired @InjectMocks private CustomAuthenticationFilter authenticationFilter;
    @Mock private AuthenticationManager authenticationManager;
    private final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/login");
    private final MockHttpServletResponse response = new MockHttpServletResponse();
    private final String tokenValue = "1";

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.request.setCookies(new Cookie(AUTHORIZATION, this.tokenValue));
    }

    @Test
    public void attemptAuthentication() throws IOException, ServletException {
        CustomAuthenticationToken unauthorizedToken = createUnauthorizedToken();
        CustomAuthenticationToken authorizedToken = createAuthorizedToken();

        when(this.authenticationManager.authenticate(unauthorizedToken)).thenReturn(authorizedToken);

        assertEquals(authorizedToken, this.authenticationFilter.attemptAuthentication(this.request, this.response));
    }

    private CustomAuthenticationToken createAuthorizedToken() {
        return new CustomAuthenticationToken(this.tokenValue, Collections.singletonList(new RoleModel(1L, "ROLE_ADMIN")));
    }

    private CustomAuthenticationToken createUnauthorizedToken() {
        return new CustomAuthenticationToken(this.tokenValue);
    }

}
