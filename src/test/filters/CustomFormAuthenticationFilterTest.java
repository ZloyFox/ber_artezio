package filters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.artezio.railway.models.RoleModel;
import ru.artezio.railway.security.CustomFormAuthenticationFilter;
import ru.artezio.railway.services.SecurityService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import java.io.IOException;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:src/main/resources/test-config.xml",
        "file:src/main/resources/security-config.xml"})
public class CustomFormAuthenticationFilterTest {

    @Autowired @Spy @InjectMocks private CustomFormAuthenticationFilter formAuthenticationFilter;
    @Mock private AuthenticationManager authenticationManager;
    @Mock private SecurityService securityService;
    private final MockHttpServletRequest request = new MockHttpServletRequest("POST", "/login");
    private final MockHttpServletResponse response = new MockHttpServletResponse();
    private final String principal = "1";
    private final String credential = "1";
    private final String tokenValue = "1";

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.request.setParameter("login", this.principal);
        this.request.setParameter("password", this.credential);
        this.request.setServletPath("/login");
    }

    @Test
    public void attemptAuthenticationUserExistsReturnAuthorizedToken() throws IOException, ServletException {
        UsernamePasswordAuthenticationToken unauthorizedToken = createUnauthorizedToken();
        UsernamePasswordAuthenticationToken authorizedToken = createAuthorizedToken();

        when(this.authenticationManager.authenticate(unauthorizedToken)).thenReturn(authorizedToken);

        assertEquals("Values must be equals", authorizedToken, this.formAuthenticationFilter.attemptAuthentication(this.request, this.response));
    }

    @Test
    public void attemptAuthenticationUserNotExistsReturnUnauthorizedToken() throws IOException, ServletException {
        UsernamePasswordAuthenticationToken unauthorizedToken = createUnauthorizedToken();

        when(this.authenticationManager.authenticate(unauthorizedToken)).thenReturn(unauthorizedToken);

        assertEquals("Values must be equals", unauthorizedToken, this.formAuthenticationFilter.attemptAuthentication(this.request, this.response));
    }

    @Test
    public void doFilterUserExistsReturnCookieWithToken() throws ServletException, IOException {
        when(this.formAuthenticationFilter.attemptAuthentication(this.request, this.response)).thenReturn(createAuthorizedToken());
        when(this.securityService.generateTokenValue(this.principal)).thenReturn(this.tokenValue);

        this.formAuthenticationFilter.doFilter(this.request, this.response, new MockFilterChain());

        assertEquals("Values must be equals", new Cookie(AUTHORIZATION, this.tokenValue).getValue(), this.response.getCookie(AUTHORIZATION).getValue());
    }

    private UsernamePasswordAuthenticationToken createAuthorizedToken() {
        return new UsernamePasswordAuthenticationToken(this.principal, this.credential, Collections.singletonList(new RoleModel(1L, "ROLE_ADMIN")));
    }

    private UsernamePasswordAuthenticationToken createUnauthorizedToken() {
        return new UsernamePasswordAuthenticationToken(this.principal, this.credential);
    }

}
