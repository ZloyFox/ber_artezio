package service;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import ru.artezio.railway.dao.UserDao;
import ru.artezio.railway.dto.UserDto;
import ru.artezio.railway.exceptions.UserNotFoundException;
import ru.artezio.railway.models.RoleModel;
import ru.artezio.railway.models.UserModel;
import ru.artezio.railway.models.UserStatus;
import ru.artezio.railway.services.SecurityService;
import ru.artezio.railway.services.UserService;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class UserServiceTest extends BaseServiceTest {

    @InjectMocks @Autowired private UserService userService;
    @Mock private SecurityService securityService;
    @Mock private UserDao userDao;

    @Test
    public void getUserExistsReturnFoundUser() {
        UserDto userDto = createUserDto(correctId, null);

        when(userDao.find(correctId)).thenReturn(map(userDto, UserModel.class));

        assertEquals("Values must be equals", userDto, userService.get(correctId));
    }

    @Test(expected = UserNotFoundException.class)
    public void getUserNotExistsThrowUserNotFoundException() {
        when(userDao.find(incorrectId)).thenReturn(null);

        userService.get(incorrectId);
    }

    @Test
    public void getListUsersExistReturnUserList() {
        List<UserDto> userDtoList = createUserDtoList();

        when(userDao.list()).thenReturn(mapToList(userDtoList, UserModel.class));

        assertEquals("Values must be equals", userDtoList, userService.list());
    }

    @Test
    public void getListUsersNotExistReturnEmptyUserList() {
        List<UserDto> userDtoList = new ArrayList<UserDto>();

        when(userDao.list()).thenReturn(mapToList(userDtoList, UserModel.class));

        assertEquals("Values must be equals", userDtoList, userService.list());
    }

    @Test
    public void persistUserNotExistsReturnIdCreatedUser() {
        UserDto userDto = new UserDto(null, "1", "1", "1", UserStatus.AWAY, null, Collections.singletonList(new RoleModel(2L, "ROLE_USER")));
        UserModel user = map(userDto, UserModel.class);

        when(securityService.encryptPassword("1")).thenReturn("1");
        when(userDao.save(user)).thenReturn(correctId);

        assertEquals("Values must be equals", correctId, userService.save(userDto));
    }

    @Test
    public void updateUserNotExistsReturnUpdatedUser() {
        UserDto userDto = createUserDto(1L, null);
        userDto.setLogin("newLogin");
        UserModel user = map(userDto, UserModel.class);

        when(userDao.update(user)).thenReturn(user);

        assertEquals("Values must be equals", userDto, userService.update(userDto));
    }

    @Test
    public void removeUserExistsReturnNothing() {
        doNothing().when(userDao).remove(correctId);

        userService.remove(correctId);
    }

    @Test(expected = UserNotFoundException.class)
    public void removeUserNotExistsThrowUserNotFoundException() {
        doThrow(new UserNotFoundException()).when(userDao).remove(incorrectId);

        userService.remove(incorrectId);
    }

    private UserDto createUserDto(Long id, String password) {
        List<RoleModel> roleModelList = new ArrayList<RoleModel>();
        roleModelList.add(new RoleModel(1L, "USER_ADMIN"));

        return new UserDto(id, id.toString(), password, id.toString(), UserStatus.ACTIVE, null, roleModelList);
    }

    private UserDto createUserDtoWithoutId() {
        UserDto userDto = new UserDto();
        List<RoleModel> roleModelList = Collections.singletonList(new RoleModel("USER_ADMIN"));
        Long randomNumber = new Random().nextLong();
        
        userDto.setLogin(randomNumber.toString());
        userDto.setPassword(randomNumber.toString());
        userDto.setEmail(randomNumber.toString());
        userDto.setStatus(UserStatus.DONT_DISTURB);
        userDto.setRoles(roleModelList);
        
        return userDto;
    }

    private List<UserDto> createUserDtoList() {
        List<UserDto> userDtoList = new ArrayList<UserDto>(4);
        List<RoleModel> roleModelList = new ArrayList<RoleModel>();
        roleModelList.add(new RoleModel(1L, "USER_ADMIN"));

        for (int i = 0; i < 3; i++) {
            Long id = (long) i;
            userDtoList.add(new UserDto(id, id.toString(), null, id.toString(), UserStatus.ACTIVE, null, roleModelList));
        }
        userDtoList.add(new UserDto(4L, "4", null, "4", UserStatus.AWAY, null, roleModelList));
        
        return userDtoList;
    }

}