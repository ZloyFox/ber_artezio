package service;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import ru.artezio.railway.dao.UserDao;
import ru.artezio.railway.dto.UserDto;
import ru.artezio.railway.exceptions.UserEmailAlreadyExistsException;
import ru.artezio.railway.exceptions.UserLoginAlreadyExistsException;
import ru.artezio.railway.models.RoleModel;
import ru.artezio.railway.models.UserModel;
import ru.artezio.railway.models.UserStatus;
import ru.artezio.railway.queries.QueryParameter;
import ru.artezio.railway.services.UserValidationService;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

public class UserValidationServiceTest extends BaseServiceTest {

    @InjectMocks @Autowired private UserValidationService userValidationService;
    @Mock private UserDao userDao;

    @Test(expected = UserLoginAlreadyExistsException.class)
    public void validateUserLoginExistsThrowUserLoginAlreadyExistsException() {
        UserModel user = createUser(1L);
        UserDto userDto = map(user, UserDto.class);

        when(userDao.findByLogin(Collections.singletonList(new QueryParameter("login", "1")))).thenReturn(user);

        userValidationService.validateUserLogin(userDto);
    }

    @Test(expected = UserEmailAlreadyExistsException.class)
    public void validateUserEmailExistsThrowUserEmailAlreadyExistsException() {
        UserModel user = createUser(1L);
        UserDto userDto = map(user, UserDto.class);

        when(userDao.findByEmail(Collections.singletonList(new QueryParameter("email", "1")))).thenReturn(user);

        userValidationService.validateUserEmail(userDto);
    }

    @Test
    public void validateUserLoginNotExistsReturnNothing() {
        when(userDao.findByLogin(Collections.singletonList(new QueryParameter("login", "1")))).thenReturn(null);

        userValidationService.validateUserLogin(map(createUser(1L), UserDto.class));
    }

    @Test
    public void validateUserEmailNotExistsReturnNothing() {
        when(userDao.findByEmail(Collections.singletonList(new QueryParameter("email", "1")))).thenReturn(null);

        userValidationService.validateUserLogin(map(createUser(1L), UserDto.class));
    }

    private UserModel createUser(Long id) {
        List<RoleModel> roles;
        if (id == 1) {
            roles = Collections.singletonList(new RoleModel(1L, "ROLE_ADMIN"));
        } else {
            roles = Collections.singletonList(new RoleModel(2L, "ROLE_USER"));
        }
        return new UserModel(id, id.toString(), id.toString(), id.toString(), UserStatus.ACTIVE, null, roles);
    }

}
