package service;

import mockit.Expectations;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import ru.artezio.railway.dao.SecurityDao;
import ru.artezio.railway.dao.UserDao;
import ru.artezio.railway.exceptions.TokenExpirationDateException;
import ru.artezio.railway.exceptions.TokenNotFoundException;
import ru.artezio.railway.exceptions.UserNotFoundException;
import ru.artezio.railway.models.RoleModel;
import ru.artezio.railway.models.TokenModel;
import ru.artezio.railway.models.UserModel;
import ru.artezio.railway.models.UserStatus;
import ru.artezio.railway.queries.QueryParameter;
import ru.artezio.railway.security.CustomAuthenticationToken;
import ru.artezio.railway.services.SecurityService;

import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static mockit.Deencapsulation.invoke;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class SecurityServiceTest extends BaseServiceTest {

    @Autowired private Date tokenExpirationDate;
    @InjectMocks @Autowired private SecurityService securityService;
    @Mock private SecurityDao securityDao;
    @Mock private UserDao userDao;
    private final String SEQUENCE = "abcdefghijklmnopqrstuvwxyz1234567890";

    @Test
    public void getByIdTokenExistsReturnFoundToken() throws ParseException {
        TokenModel token = createToken(1L, this.tokenExpirationDate);

        when(this.securityDao.findById("1")).thenReturn(token);

        assertEquals("Values must be equals", token, this.securityService.getById("1"));
    }

    @Test(expected = UserNotFoundException.class)
    public void getByIdTokenNotExistsThrowTokenNotFoundException() {
        when(this.securityDao.findById("0")).thenThrow(UserNotFoundException.class);

        securityService.getById("0");
    }

    @Test
    public void authorizeUserExistsReturnGeneratedTokenValue() throws Exception {
        TokenModel token = createToken(1L, this.tokenExpirationDate);
        CustomAuthenticationToken authenticationToken = new CustomAuthenticationToken("1");
        CustomAuthenticationToken expectedAuthenticationToken = new CustomAuthenticationToken("1", Collections.singletonList(new RoleModel(1L, "ROLE_ADMIN")));
        UserModel user = createUser(1L);

        when(securityDao.findById("1")).thenReturn(token);
        when(userDao.find(token.getUserId())).thenReturn(user);

        assertEquals("Values must be equals", expectedAuthenticationToken, securityService.authorize(authenticationToken));
    }

    @Test(expected = TokenExpirationDateException.class)
    public void authorizeTokenExpiredThrowTokenExpirationException() throws ParseException {
        TokenModel token = createToken(1L, new Date(1000));
        CustomAuthenticationToken authenticationToken = new CustomAuthenticationToken("1");

        when(securityDao.findById("1")).thenReturn(token);

        securityService.authorize(authenticationToken);
    }

    @Test(expected = TokenNotFoundException.class)
    public void authorizeTokenNotExistsThrowTokenNotFoundException() throws Exception {
        CustomAuthenticationToken authenticationToken = new CustomAuthenticationToken("0");

        when(securityDao.findById("0")).thenReturn(null);

        securityService.authorize(authenticationToken);
    }

    @Test(expected = UserNotFoundException.class)
    public void authorizeUserNotExistsThrowUserNotFoundException() throws ParseException {
        TokenModel token = createToken(0L, this.tokenExpirationDate);
        CustomAuthenticationToken authenticationToken = new CustomAuthenticationToken("1");

        when(securityDao.findById("1")).thenReturn(token);
        when(userDao.find(0L)).thenReturn(null);

        securityService.authorize(authenticationToken);

    }

    @Test
    public void generateTokenValueUserExistsReturnGeneratedTokenValue() throws ParseException {
        UserModel user = createUser(1L);
        TokenModel token = createToken(1L, this.tokenExpirationDate);
        List<QueryParameter> parameters = Collections.singletonList(new QueryParameter("login", "1"));

        when(userDao.findByLogin(parameters)).thenReturn(user);
        new Expectations(RandomStringUtils.class) {{
            invoke(RandomStringUtils.class, "random", 36, SEQUENCE); returns("1");
        }};
        when(securityDao.save(token)).thenReturn("1");

        assertEquals("Values must be equals", "1", securityService.generateTokenValue("1"));
    }

    @Test(expected = UserNotFoundException.class)
    public void generateTokenValueUserNotExistsThrowUserNotFoundException() {
        List<QueryParameter> parameters = Collections.singletonList(new QueryParameter("login", "0"));

        when(userDao.findByLogin(parameters)).thenReturn(null);

        securityService.generateTokenValue("0");
    }

    private UserModel createUser(Long id) {
        List<RoleModel> roles;
        if (id == 1) {
            roles = Collections.singletonList(new RoleModel(1L, "ROLE_ADMIN"));
        } else {
            roles = Collections.singletonList(new RoleModel(2L, "ROLE_USER"));
        }
        return new UserModel(id, id.toString(), id.toString(), id.toString(), UserStatus.ACTIVE, null, roles);
    }

    private TokenModel createToken(Long userId, Date expirationDate) throws ParseException {
        return new TokenModel(userId.toString(), userId, expirationDate);
    }

}
