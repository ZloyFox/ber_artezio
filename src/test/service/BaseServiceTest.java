package service;

import org.dozer.Mapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:src/main/resources/test-config.xml",
        "file:src/main/resources/security-config.xml"})
public abstract class BaseServiceTest {

    @Autowired private Mapper dozerMapper;
    final Long correctId = 1L;
    final Long incorrectId = -1L;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    <T, U> T map(U object, Class<T> typeClass) {
        return dozerMapper.map(object, typeClass);
    }

    <T, U> List<T> mapToList(List<U> objectList, Class<T> typeClass) {
        List<T> resultList = new ArrayList<T>(objectList.size());
        for (U object : objectList) {
            resultList.add(dozerMapper.map(object, typeClass));
        }
        return resultList;
    }

}