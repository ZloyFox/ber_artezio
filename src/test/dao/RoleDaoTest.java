package dao;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.artezio.railway.dao.RoleDao;
import ru.artezio.railway.exceptions.RoleNotFoundException;
import ru.artezio.railway.models.RoleModel;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class RoleDaoTest extends BaseDaoTest {

    @Autowired private RoleDao roleDao;

    @Test
    public void getRoleExistsReturnFoundRoleModel() {
        RoleModel roleModel = new RoleModel(1L, "ROLE_ADMIN");

        assertEquals("Values must be equals", roleModel, roleDao.find(correctId));
    }

    @Test
    public void getRoleNotExistsReturnNull() {
        assertNull("Value must be null", roleDao.find(incorrectId));
    }

    @Test
    public void getListByNamedQueryRolesExistsReturnRoleList() {
        assertEquals("Values must be equals", buildRoleModelList(), roleDao.list());
    }

    @Test
    public void persistRoleNotExistsReturnIdCreatedRoleModel() {
        RoleModel roleModel = new RoleModel();
        roleModel.setName("TestRoleModel");

        assertNotNull("Value must not be null", roleDao.save(roleModel));
    }

    @Test
    public void updateRoleNotExistsReturnUpdatedRoleModel() {
        RoleModel roleModel = new RoleModel();
        roleModel.setId(1L);
        roleModel.setName("newName");

        assertEquals("Values must be equals", roleModel, roleDao.update(roleModel));
    }

    @Test
    public void removeRoleExistsFindMustReturnNull() {
        roleDao.remove(correctId);

        assertNull("Value must be null", entityManager.find(RoleModel.class, correctId));
    }

    @Test(expected = RoleNotFoundException.class)
    public void removeRoleNotExistsThrowRoleNotFoundException() {
        roleDao.remove(incorrectId);
    }

    private List<RoleModel> buildRoleModelList() {
        List<RoleModel> roleModelList = new ArrayList<RoleModel>(2);
        roleModelList.add(new RoleModel(1L, "ROLE_ADMIN"));
        roleModelList.add(new RoleModel(2L, "ROLE_USER"));

        return roleModelList;
    }

}