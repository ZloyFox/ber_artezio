package dao;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.artezio.railway.dao.SecurityDao;
import ru.artezio.railway.models.TokenModel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SecurityDaoTest extends BaseDaoTest {

    @Autowired private SecurityDao securityDao;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    @Test
    public void findByValueTokenExistsReturnFoundToken() throws ParseException {
        TokenModel token = new TokenModel("1", 1L, simpleDateFormat.parse("2018-04-19 09:00:32"));

        assertEquals("Values must be equals", token, securityDao.findById("1"));
    }

    @Test
    public void findByValueTokenNotExistsReturnNull() {
        assertNull("Value must be null", securityDao.findById("0"));
    }

    @Test
    public void persistTokenNotExistsReturnIdFoundToken() throws ParseException {
        TokenModel token = new TokenModel("newId", 1L, new Date());

        securityDao.save(token);

        assertEquals("Values must be equals", token, entityManager.find(TokenModel.class, token.getId()));
    }

}
