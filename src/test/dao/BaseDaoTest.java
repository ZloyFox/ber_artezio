package dao;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "file:src/main/resources/test-config.xml",
        "file:src/main/resources/security-config.xml"})
@Transactional
public abstract class BaseDaoTest {

    @PersistenceContext EntityManager entityManager;
    final Long correctId = 1L;
    final Long incorrectId = -1L;

    @Before
    public void initDB() {
        this.entityManager.createNativeQuery("CREATE TABLE users (\n" +
                "  id    INTEGER PRIMARY KEY AUTO_INCREMENT,\n" +
                "  login  VARCHAR(20),\n" +
                "  password VARCHAR(255),\n" +
                "  email VARCHAR(20),\n" +
                "  status VARCHAR(20)\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE user_info (\n" +
                "  user_id INTEGER PRIMARY KEY AUTO_INCREMENT,\n" +
                "  first_name VARCHAR(50),\n" +
                "  middle_name VARCHAR(50),\n" +
                "  last_name VARCHAR(50),\n" +
                "  age INT,\n" +
                "  city VARCHAR(50)\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE roles (\n" +
                "  id INTEGER PRIMARY KEY AUTO_INCREMENT,\n" +
                "  name VARCHAR(20)\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE user_roles (\n" +
                "  id INTEGER PRIMARY KEY AUTO_INCREMENT,\n" +
                "  user_id INT,\n" +
                "  role_id INT\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE user_tokens (\n" +
                "  id VARCHAR(50) PRIMARY KEY ,\n" +
                "  expiration_date DATETIME,\n" +
                "  user_id INT\n" +
                ");\n" +
                "\n" +
                "INSERT INTO users (id, login, password, email, status) VALUES (1, '1', '1', '1', 'ACTIVE');\n" +
                "INSERT INTO users (id, login, password, email, status) VALUES (2, '2', '2', '2', 'ACTIVE');\n" +
                "\n" +
                "INSERT INTO user_info (user_id, first_name, middle_name, last_name, age, city) VALUES (1, '1', '1', '1', 1, 'city');\n" +
                "INSERT INTO user_info (user_id, first_name, middle_name, last_name, age, city) VALUES (2, '2', '2', '2', 2, 'city');\n" +
                "\n" +
                "INSERT INTO roles (id, name) VALUES (1, 'ROLE_ADMIN');\n" +
                "INSERT INTO roles (id, name) VALUES (2, 'ROLE_USER');\n" +
                "\n" +
                "INSERT INTO user_roles (id, user_id, role_id) VALUES (1, 1, 1);\n" +
                "INSERT INTO user_roles (id, user_id, role_id) VALUES (2, 2, 2);\n" +
                "\n" +
                "INSERT INTO user_tokens (id, expiration_date, user_id) VALUES ('1', '2018-04-19 09:00:32', 1);\n" +
                "INSERT INTO user_tokens (id, expiration_date, user_id) VALUES ('2', '2018-04-21 09:00:32', 2);").executeUpdate();
    }

}