package dao;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.artezio.railway.dao.UserDao;
import ru.artezio.railway.models.RoleModel;
import ru.artezio.railway.models.UserInfoModel;
import ru.artezio.railway.models.UserModel;
import ru.artezio.railway.models.UserStatus;
import ru.artezio.railway.queries.QueryParameter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class UserDaoTest extends BaseDaoTest {

    @Autowired private UserDao userDao;

    @Test
    public void findUserExistsReturnFoundUser() {
        UserModel userModel = createUser(1L);

        assertEquals("Values must be equals", userModel, userDao.find(correctId));
    }

    @Test
    public void findUserNotExistsReturnNull() {
        assertNull("Value must be null", userDao.find(incorrectId));
    }

    @Test
    public void findListByNamedQueryUsersExistReturnUserList() {
        List<UserModel> userModels = createUserList(2, UserStatus.ACTIVE);
        QueryParameter queryParameter = new QueryParameter("status", UserStatus.ACTIVE);

        assertEquals("Values must be equals", userModels, userDao.findListByParams(Collections.singletonList(queryParameter)));
    }

    @Test
    public void findListUsersExistReturnUserList() {
        List<UserModel> userModels = createUserList(2, UserStatus.ACTIVE);

        assertEquals("Values must be equals", userModels, userDao.list());
    }

    @Test
    public void saveUserNotExistsReturnIdCreatedUser() {
        UserModel userModel = createUserWithoutId();

        userDao.save(userModel);

        assertEquals("Value must not be null", userModel, entityManager.find(UserModel.class, userModel.getId()));
    }

    @Test
    public void updateUserNotExistsReturnUpdatedUser() {
        UserModel userModel = createUser(1L);
        userModel.setLogin("newLogin");

        assertEquals("Values must be equals", userModel, userDao.update(userModel));
    }

    @Test
    public void removeUserExistsFindMustReturnNull() {
        userDao.remove(1L);

        assertNull("Value must be null", entityManager.find(UserModel.class, 1L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeUserNotExistsThrowIllegalArgumentException() {
        userDao.remove(incorrectId);
    }

    private UserModel createUser(Long id) {
        List<RoleModel> roles;
        UserInfoModel userInfo = new UserInfoModel(id, id.toString(), id.toString(), id.toString(), id.intValue(), "city");
        if (id == 1) {
            roles = Collections.singletonList(new RoleModel(1L, "ROLE_ADMIN"));
        } else {
            roles = Collections.singletonList(new RoleModel(2L, "ROLE_USER"));
        }
        return new UserModel(id, id.toString(), id.toString(), id.toString(), UserStatus.ACTIVE, userInfo, roles);
    }

    private UserModel createUserWithoutId() {
        UserModel userModel = new UserModel();
        Long randomNumber = new Random().nextLong();

        userModel.setLogin(randomNumber.toString());
        userModel.setPassword(randomNumber.toString());
        userModel.setEmail(randomNumber.toString());
        userModel.setStatus(null);
        userModel.setRoles(Collections.singletonList(new RoleModel(1L, "ROLE_USER")));
        return userModel;
    }

    private List<UserModel> createUserList(int size, UserStatus userStatus) {
        List<RoleModel> roles = Collections.singletonList(new RoleModel(2L, "ROLE_USER"));
        List<UserModel> userModels = new ArrayList<UserModel>(4);

        userModels.add(createUser(1L));
        for (int i = 2; i <= size; i++) {
            UserInfoModel userInfo = new UserInfoModel((long) i, String.valueOf(i), String.valueOf(i), String.valueOf(i), i, "city");
            userModels.add(new UserModel((long) i, String.valueOf(i), String.valueOf(i), String.valueOf(i), userStatus, userInfo, roles));
        }
        return userModels;
    }

}